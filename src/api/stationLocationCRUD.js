import instance from "../utils/Service";
// Station Location
const Api = {
  fetchstationlocation: () => {
    const url = "/Stations";
    return instance.get(url);
  },
  createstationlocation: value => {
    const url = "/Stations";
    return instance.post(url, value.data);
  },
  updatestationlocation: value => {
    console.log("API => updatestationlocation()  recieved value is ", value);
    const url = "/Stations";
    return instance.patch(url, value.data);
  },
  deletestationlocation: value => {
    console.log("API => deletestationlocation()  recieved value is ", value);
    const url = `/Stations/${value.data.id}`;
    return instance.delete(url);
  }
};
export default Api;
