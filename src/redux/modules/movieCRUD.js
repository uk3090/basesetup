import {
  FETCH_MOVIE_SUCCEEDED,
  ADD_NEW_MOVIE_SUCCEEDED,
  EDIT_MOVIE,
  UPDATE_MOVIE_SUCCEEDED,
  CLEAR_MOVIE,
  DELETE_MOVIE_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  movieList: [],
  formValues: [],
  movieEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  Movies -------------------------------

    case FETCH_MOVIE_SUCCEEDED:
      return {
        ...state,
        movieList: action.data
      };
    case ADD_NEW_MOVIE_SUCCEEDED:
      const newmovieList = [...state.movieList];
      newmovieList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        movieList: newmovieList
      };
    case UPDATE_MOVIE_SUCCEEDED:
      const objIndex = state.movieList.findIndex(
        obj => obj.id === action.data.data.id
      );

      // make new object of updated object.
      const updatedObj = { ...action.data.data };

      // make final new array of objects by combining updated object.
      const updatedmovieList = [
        ...state.movieList.slice(0, objIndex),
        updatedObj,
        ...state.movieList.slice(objIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        movieList: updatedmovieList
      };
    case EDIT_MOVIE:
      let movie = { ...action.data };
      movie.release_date = new Date(action.data.release_date);
      let technicians = [];
      let actors = [];
      action.data.technicians.map(val => {
        technicians.push({ label: val, value: val });
      });
      action.data.actors.map(val => {
        actors.push({ label: val, value: val });
      });
      movie.technicians = technicians;
      movie.actors = actors;
      return {
        ...state,
        movieEdit: movie
      };
    case CLEAR_MOVIE:
      return {
        ...state,
        movieEdit: {}
      };
    case DELETE_MOVIE_SUCCEEDED:
      const delIndex = state.movieList.findIndex(
        obj => obj.id === action.data.data.id
      );

      const afterDeletemovieList = [
        ...state.movieList.slice(0, delIndex),
        ...state.movieList.slice(delIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        movieList: afterDeletemovieList
      };

    default:
      return state;
  }
}
