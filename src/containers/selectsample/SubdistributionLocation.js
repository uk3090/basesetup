import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";

import {
  renderTextField,
  renderSelectField
} from "../../components/FormFields";

class MaterialUiForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distributionLocationList: []
    };
  }
  static getDerivedStateFromProps(props, state) {
    let distributionList = [];
    props.distributionLocationList.map(list => {
      distributionList.push({ label: list.name, value: list.id });
    });
    return {
      distributionLocationList: distributionList
    };
  }

  render() {
    const {
      handleSubmit,
      pristine,
      reset,
      submitting,
      onSubmit,
      invalid,
      openForm,
      closeForm,
      classes
    } = this.props;
    const { distributionLocationList } = this.state;
    console.log("distributionLocationList");
    console.log(distributionLocationList);

    return (
      <FloatingPanel openForm={openForm} closeForm={closeForm}>
        <form
          className={classes.distributionLocationForm}
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className={classes.fields}>
            <Field
              name="name"
              component={renderTextField}
              label="DistributionLocation Name"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="distribution"
              component={renderSelectField}
              options={distributionLocationList}
              label="Select"
            />
          </div>

          <div>
            <Button
              className={classes.submitButton}
              type="submit"
              variant="contained"
              disabled={invalid || pristine || submitting}
            >
              Submit
            </Button>
            <Button
              className={classes.cleatButton}
              variant="contained"
              disabled={pristine || submitting}
              onClick={reset}
            >
              Clear
            </Button>
          </div>
        </form>
      </FloatingPanel>
    );
  }
}

const Subdistribution = reduxForm({
  form: "CreateSubdistribution", // a unique identifier for this form
  validate
})(MaterialUiForm);

export default compose(withStyles(formStyle))(Subdistribution);
