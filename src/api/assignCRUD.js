import instance from "../utils/Service";

const Api = {
  //assign
  fetchassign: () => {
    const url = "/Movies/assign";
    return instance.get(url);
  },
  createassign: value => {
    const url = "/Movies/assign";
    return instance.post(url, value.data);
  },
  updateassign: value => {
    const url = "/Movies/assign";
    return instance.post(url, value.data); //post for mokeable purpose change to patch
  },
  deleteassign: value => {
    console.log("API => deleteassign()  recieved value is ", value);

    const url = `/Movies/assign`;
    return instance.delete(url);
  }
};

export default Api;
