const validate = values => {
  const errors = {company:{},user:{}};

    if (values.company && !values.company.companyName) {
      errors.company.companyName = "Required";
    }
    if (values.company && !values.company.address) {
      errors.company.address = "Required";
    }
    if (values.company &&  !values.company.email) {
      errors.company.email = "Required";
    } else if(values.company){
      if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.company.email)) {
        errors.company.email = "Invalid email address";
      }
    }
    
  
    if (values.company && !values.company.phoneNumber) {
      errors.company.phoneNumber = "Required";
    }
    if (values.company && !values.company.website) {
      errors.company.website = "Required";
    }
    if (values.user && !values.user.username) {
      errors.user.username = 'Required'
    }
    if (values.user && !values.user.mobileNumber) {
      errors.user.mobileNumber = 'Required'
    } else if(values.user && values.user.mobileNumber.length < 10) {
      errors.user.mobileNumber = 'Invalid'
    }
    if(values.company && !values.company.id) {
      if(values.user &&  !values.user.password) {
        errors.user.password = 'Required'
      }
      if(values.user &&  !values.user.confirmPassword) {
        errors.user.confirmPassword = 'Required'
      }else if(values.user &&  values.user.confirmPassword !== values.user.password) {
        errors.user.confirmPassword = 'Match error'
      }
    }

  return errors;
};

export default validate;
