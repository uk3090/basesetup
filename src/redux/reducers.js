import { combineReducers } from "redux";
import { reducer as reduxFormReducer } from "redux-form";

import { connectRouter } from "connected-react-router";
import admin from "./modules/admin";
import companyCRUD from "./modules/companyCRUD";
import collectionCRUD from "./modules/collectionCRUD";
import userCRUD from "./modules/userCRUD";
import movieCRUD from "./modules/movieCRUD";
import theatreCRUD from "./modules/theatreCRUD";
import theatreAgreementCRUD from "./modules/theatreAgreementCRUD";
import scheduleCRUD from "./modules/scheduleCRUD";
import distributionLocationCRUD from "./modules/distributionLocationCRUD";
import subDistributionLocationCRUD from "./modules/subDistributionLocationCRUD";
import stationLocationCRUD from "./modules/stationLocationCRUD";
import assignCRUD from "./modules/assignCRUD";
import sidebar from "./modules/sidebarReducer";
import user from "./modules/login";
import collection from "./modules/collection";

export default history =>
  combineReducers({
    form: reduxFormReducer,
    admin,
    companyCRUD,
    userCRUD,
    movieCRUD,
    theatreCRUD,
    theatreAgreementCRUD,
    scheduleCRUD,
    distributionLocationCRUD,
    subDistributionLocationCRUD,
    stationLocationCRUD,
    collectionCRUD,
    assignCRUD,
    sidebar,
    user,
    collection,
    router: connectRouter(history)
  });
