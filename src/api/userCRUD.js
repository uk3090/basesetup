// import { mockable_instance } from "../utils/Service";
import instance from "../utils/Service";
const Api = {
  fetchUser: () => {
    const url = "/Employees";
    return instance.get(url);
  },
  createUser: value => {
    const url = "/Users";
    return instance.post(url, value.data);
  },
  updateUser: value => {
    const url = "/Users";
    return instance.post(url, value.data); //post for mokeable purpose change to patch
  },
  deleteUser: value => {
    const url = `/Users`;
    return instance.delete(url);
  }
};

export default Api;
