import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";
import {
  renderSelectField,
  DateTimePickerRow
} from "../../components/FormFields";

const MaterialUiForm = props => {
  const {
    handleSubmit,
    subDistributionLocationList,
    movieList,
    companyList,
    theatreList,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;

  let subDistributionLocations = [];
  let movies = [];
  let companies = [];
  let theatres = [];

  subDistributionLocationList.map(value => {
    subDistributionLocations.push({
      label: value.name,
      value: value.id
    });
  });
  movieList.map(value => {
    movies.push({
      label: value.name,
      value: value.id
    });
  });
  companyList.map(value => {
    companies.push({
      label: value.name,
      value: value.id
    });
  });
  theatreList.map(value => {
    theatres.push({
      label: value.name,
      value: value.id
    });
  });

  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      <form
        className={classes.stationLocationForm}
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="subdistributionLocation"
            component={renderSelectField}
            options={subDistributionLocationList}
            label="Distribution Area"
            getOptionLabel={option =>
              `${option.name}`
            }
            getOptionValue={option =>
              `${option.id}`
            }
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="movie"
            component={renderSelectField}
            options={movieList}
            getOptionLabel={option =>
              `${option.name}`
            }
            getOptionValue={option =>
              `${option.id}`
            }
            label="Select movie"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="theater"
            component={renderSelectField}
            options={theatreList}
            getOptionLabel={option =>
              `${option.name}`
            }
            getOptionValue={option =>
              `${option.id}`
            }
            label="Select Theatre"
          />
        </div>
        <div className={classes.fields}>
          <Field
            name="from_date"
            label="From date"
            component={DateTimePickerRow}
            dateFormat="dd/MM/yyyy"
          />
        </div>
        <div className={classes.fields}>
          <Field
            name="to_date"
            component={DateTimePickerRow}
            label="To Date"
            dateFormat="dd/MM/yyyy"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="producer"
            component={renderSelectField}
            options={companyList}
            label="Select Producer"
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
            menuPlacement="top"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="distribution"
            component={renderSelectField}
            options={companyList}
            label="Select Distributor"
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
            menuPlacement="top"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="subdistribution"
            component={renderSelectField}
            options={companyList}
            label="Select Sub Distributor"
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
            menuPlacement="top"
          />
        </div>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid || pristine || submitting}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let CreateScheduleForm = reduxForm({
  form: "CreateSchedule", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(MaterialUiForm);

CreateScheduleForm = connect(state => ({
  initialValues: state.scheduleCRUD.scheduleEdit || {}
}))(CreateScheduleForm);

export default compose(withStyles(formStyle))(CreateScheduleForm);
