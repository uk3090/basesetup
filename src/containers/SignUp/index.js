import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
// import Input from '@material-ui/core/Input';
// import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from "./styles/styles";
import { Field, reduxForm } from 'redux-form'
import { renderTextField } from "../../components/TextField";

import asyncValidate from './asyncValidate'
import validate from "./validate";

function SignUp(props) {
  const { classes } = props;
  return (
    <main className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <form className={classes.form}>
          <FormControl margin="normal" required fullWidth>
            <Field name="name" component={renderTextField} label="Name" />
          </FormControl>
        <FormControl margin="normal" required fullWidth>
            <Field name="number" component={renderTextField} label="Number" type="number" />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <Field name="email" component={renderTextField} label="Email" type="email" />
          </FormControl>
         
          <FormControl margin="normal" required fullWidth>
            <Field name="password" component={renderTextField} label="Password" type="password" />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <Field name="cofirmPassword" component={renderTextField} label="Cofirm Password" type="Password" />
            
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
        </form>
      </Paper>
    </main>
  );
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default reduxForm({
  form: 'signUp', // a unique identifier for this form
  validate,
  asyncValidate
})(withStyles(styles)(SignUp))