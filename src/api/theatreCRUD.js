import instance from "../utils/Service";
const Api = {
  //theatre
  fetchtheatre: () => {
    const url = "/Theaters";
    return instance.get(url);
  },
  createtheatre: value => {
    const url = "/Theaters";
    return instance.post(url, value.data);
  },
  updatetheatre: value => {
    const url = "/Theaters";
    return instance.patch(url, value.data);
  },
  deletetheatre: value => {
    console.log("API => deletetheare()  recieved value is ", value.data);
    const url = `/Theaters/${value.id}`;
    return instance.delete(url);
  }
};
export default Api;
