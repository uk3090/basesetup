import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/distributionLocationCRUD";
import {
  updateDistributionLocationSuccess,
  deleteDistributionLocationSuccess,
  fetchDistributionLocationSuccess,
  createDistributionLocationSuccess
} from "../actions/admin";
import {
  FETCH_DISTRIBUTIONLOCATION,
  ADD_NEW_DISTRIBUTIONLOCATION,
  UPDATE_DISTRIBUTIONLOCATION,
  DELETE_DISTRIBUTIONLOCATION
} from "../actions/actionConstants";

// Distribution Location
export function* createDistributionLocation(value) {
  try {
    console.log("Create Distribution Location Saga Called");
    const response = yield call(Api.createdistributionlocation, value);
    yield put(createDistributionLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateDistributionLocation(value) {
  try {
    console.log("update DistributionLocation Saga Called");
    const response = yield call(Api.updatedistributionlocation, value);
    yield put(updateDistributionLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteDistributionLocation(value) {
  try {
    console.log("delete DistributionLocation Saga Called");
    yield call(Api.deletedistributionlocation, value);
    yield put(deleteDistributionLocationSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchDistributionLocation() {
  try {
    console.log("Fetch Distribution Location Saga Called");
    const response = yield call(Api.fetchdistributionlocation);
    yield put(fetchDistributionLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const distributionLocationCRUD = [
  takeLatest(FETCH_DISTRIBUTIONLOCATION, fetchDistributionLocation),
  takeLatest(ADD_NEW_DISTRIBUTIONLOCATION, createDistributionLocation),
  takeLatest(UPDATE_DISTRIBUTIONLOCATION, updateDistributionLocation),
  takeLatest(DELETE_DISTRIBUTIONLOCATION, deleteDistributionLocation)
];
