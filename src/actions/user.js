import { LOGIN_SUCCEEDED, LOGOUT_SUCCEEDED } from "./actionConstants";

export const loginSuccess = data => ({
  type: LOGIN_SUCCEEDED,
  data
});

export const logoutSuccess = () => ({
  type: LOGOUT_SUCCEEDED
});
