import {
  FETCH_SCHEDULE_SUCCEEDED,
  ADD_NEW_SCHEDULE_SUCCEEDED,
  UPDATE_SCHEDULE_SUCCEEDED,
  EDIT_SCHEDULE,
  CLEAR_SCHEDULE,
  DELETE_SCHEDULE_SUCCEEDED
} from "../../actions/actionConstants";

import moment from 'moment';

const initialState = {
  formValues: [],
  scheduleList: [],
  scheduleEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  Schedule -------------------------------

    case FETCH_SCHEDULE_SUCCEEDED:
      return {
        ...state,
        scheduleList: action.data
      };
    case ADD_NEW_SCHEDULE_SUCCEEDED:
      const scheduleList = state.scheduleList;
      scheduleList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        scheduleList
      };
    case UPDATE_SCHEDULE_SUCCEEDED:
      const scheduleobjIndex = state.scheduleList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const scheduleupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const updatedscheduleList = [
        ...state.scheduleList.slice(0, scheduleobjIndex),
        scheduleupdatedObj,
        ...state.scheduleList.slice(scheduleobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        scheduleList: updatedscheduleList
      };
    case EDIT_SCHEDULE:
      console.log("----------EDIT_schedule------------");
      console.log(action.data);
      console.log("----------------------");
      const scheduleEdit = { ...action.data }
      scheduleEdit.from_date = new Date(action.data.from_date);
      scheduleEdit.to_date = new Date(action.data.to_date);
      return {
        ...state,
        scheduleEdit: scheduleEdit
      };
    case CLEAR_SCHEDULE:
      return {
        ...state,
        scheduleEdit: {}
      };
    case DELETE_SCHEDULE_SUCCEEDED:
      const scheduleindex = state.scheduleList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const schedulerecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletescheduleList = [
        ...state.scheduleList.slice(0, scheduleindex),
        schedulerecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        scheduleList: afterDeletescheduleList
      };
    default:
      return state;
  }
}
