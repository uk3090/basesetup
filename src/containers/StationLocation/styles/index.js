const styles = theme => ({
  stationLocationContainor: {
    margin: "100px",
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  stationLocationTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createStationLocation: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
