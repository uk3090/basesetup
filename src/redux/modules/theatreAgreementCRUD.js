import {
  FETCH_AGREEMENT_SUCCEEDED,
  ADD_NEW_AGREEMENT_SUCCEEDED,
  UPDATE_AGREEMENT_SUCCEEDED,
  EDIT_AGREEMENT,
  CLEAR_AGREEMENT,
  DELETE_AGREEMENT_SUCCEEDED
} from "../../actions/actionConstants";
const initialState = {
  agreementList: [],
  agreementEdit: {},
  formValues: [],
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    //----------------------- Agreement --------------------------------------

    case FETCH_AGREEMENT_SUCCEEDED:
      return {
        ...state,
        agreementList: action.data
      };
    case ADD_NEW_AGREEMENT_SUCCEEDED:
      const agreementList = state.agreementList;
      agreementList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        agreementList
      };
    case UPDATE_AGREEMENT_SUCCEEDED:
      const agreementListobjIndex = state.agreementList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const agreementListupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const agreementListupdatedAgreementList = [
        ...state.agreementList.slice(0, agreementListobjIndex),
        agreementListupdatedObj,
        ...state.agreementList.slice(agreementListobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        agreementList: agreementListupdatedAgreementList
      };
    case EDIT_AGREEMENT:
      const agreementEdit = { ...action.data };
      agreementEdit.created_date = new Date(action.data.created_date);
      agreementEdit.type = { label: `Type${action.data.type}`, value: action.data.type };
      agreementEdit.share = [];
      action.data.share.map(val => {
        agreementEdit.share.push({ label: val, value: val })
      })
      return {
        ...state,
        agreementEdit: agreementEdit
      };
    case CLEAR_AGREEMENT:
      return {
        ...state,
        agreementEdit: {}
      };
    case DELETE_AGREEMENT_SUCCEEDED:
      const agreementListindex = state.agreementList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const agreementListrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const agreementListafterDeleteAgreementList = [
        ...state.companyList.slice(0, agreementListindex),
        agreementListrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        agreementList: agreementListafterDeleteAgreementList
      };
    default:
      return state;
  }
}
