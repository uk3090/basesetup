const Report = {
  movie: {
    id: 234,
    name: "Sarker",
    producer: "Thanu S"
  },
  card: [
    {
      distArea: "TN",
      gross: 29300,
      distCompanyName: "Some company"
    },
    {
      distArea: "KL",
      gross: 29300,
      distCompanyName: "Some company"
    }
  ],
  chart: {
    label: ["TN", "KL", "AN", "NS"],
    data: ["7200", "1289", "2312", "4211"]
  },
  table: [
    {
      distArea: "TN",
      Gross: 12110,
      Share: 12332,
      net: 22133,
      dedution: 13233
    },
    {
      distArea: "KL",
      Gross: 12110,
      Share: 12332,
      net: 22133,
      dedution: 13233
    }
  ]
};
export default Report;
