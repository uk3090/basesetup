import { put, call, takeLatest } from "redux-saga/effects";
import {
  FETCH_MOVIE,
  ADD_NEW_MOVIE,
  UPDATE_MOVIE,
  DELETE_MOVIE
} from "../actions/actionConstants";
import Api from "../api/movieCRUD";
import {
  fetchMovieSuccess,
  createMovieSuccess,
  updateMovieSuccess,
  deleteMovieSuccess
} from "../actions/admin";

// Movie
export function* fetchMovie() {
  try {
    const response = yield call(Api.fetchMovie);
    yield put(fetchMovieSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* createMovie(value) {
  try {
    yield call(Api.createMovie, value);
    yield put(createMovieSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* updateMovie(value) {
  try {
    yield call(Api.updateMovie, value);
    yield put(updateMovieSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteMovie(value) {
  try {
    yield call(Api.deleteMovie, value);
    yield put(deleteMovieSuccess(value));
  } catch (error) {
    console.log(error);
  }
}

export const movieCRUD = [
  takeLatest(FETCH_MOVIE, fetchMovie),
  takeLatest(ADD_NEW_MOVIE, createMovie),
  takeLatest(UPDATE_MOVIE, updateMovie),
  takeLatest(DELETE_MOVIE, deleteMovie)
];
