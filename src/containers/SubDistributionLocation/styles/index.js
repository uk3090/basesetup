const styles = theme => ({
  subDistributionLocationContainor: {
    margin: "100px",
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  subDistributionLocationTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createSubDistributionLocation: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
