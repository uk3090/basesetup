import {
  FETCH_STATIONLOCATION_SUCCEEDED,
  ADD_NEW_STATIONLOCATION_SUCCEEDED,
  UPDATE_STATIONLOCATION_SUCCEEDED,
  EDIT_STATIONLOCATION,
  CLEAR_STATIONLOCATION,
  DELETE_STATIONLOCATION_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  formValues: [],
  stationLocationList: [],
  stationLocationEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  Schedule -------------------------------

    case FETCH_STATIONLOCATION_SUCCEEDED:
      return {
        ...state,
        stationLocationList: action.data
      };
    case ADD_NEW_STATIONLOCATION_SUCCEEDED:
      const stationLocationList = state.stationLocationList;
      stationLocationList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        stationLocationList
      };
    case UPDATE_STATIONLOCATION_SUCCEEDED:
      const stationLocationobjIndex = state.stationLocationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const stationLocationupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const updatedstationLocationList = [
        ...state.stationLocationList.slice(0, stationLocationobjIndex),
        stationLocationupdatedObj,
        ...state.stationLocationList.slice(stationLocationobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        stationLocationList: updatedstationLocationList
      };
    case EDIT_STATIONLOCATION:
      console.log("----------EDIT_COMPANY------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        stationLocationEdit: action.data
      };
    case CLEAR_STATIONLOCATION:
      return {
        ...state,
        stationLocationEdit: {}
      };
    case DELETE_STATIONLOCATION_SUCCEEDED:
      const stationLocationindex = state.scheduleList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const stationLocationrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletestationLocationList = [
        ...state.stationLocationList.slice(0, stationLocationindex),
        stationLocationrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        stationLocationList: afterDeletestationLocationList
      };
    default:
      return state;
  }
}
