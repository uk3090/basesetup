import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Field } from "redux-form";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { renderTextField } from "../../components/FormFields";
import styles from "./styles/styles";

const renderSlots = ({ fields, classes, meta: { error, submitFailed } }) => (
  <ul className={classes.multifields}>
    <li>
      <Button
        size="small"
        variant="outlined"
        color="primary"
        onClick={() => fields.push({})}
      >
        Add Slot
      </Button>
      {submitFailed && error && <span>{error}</span>}
    </li>
    {fields.map((slot, index) => (
      <li key={index}>
        <Grid container md={12} xs={12}>
          <Grid item md={2} xs={6} className={classes.multifieldsGrid}>
            <h4>Slot #{index + 1}</h4>
          </Grid>
          <Grid item md={2} xs={6} className={classes.multifieldsGrid}>
            <Field
              name={`${slot}.ticket_rate`}
              type="text"
              component={renderTextField}
              label="Rate"
            />
          </Grid>
          <Grid item md={2} xs={6} className={classes.multifieldsGrid}>
            <Field
              name={`${slot}.count`}
              type="text"
              component={renderTextField}
              label="slot"
            />
          </Grid>
          <Grid item md={2} xs={6} className={classes.multifieldsGrid}>
            <IconButton
              aria-label="Delete"
              className={classes.margin}
              onClick={() => fields.remove(index)}
            >
              <DeleteIcon fontSize="small" />
            </IconButton>
          </Grid>
        </Grid>
      </li>
    ))}
  </ul>
);

export default withStyles(styles)(renderSlots);
