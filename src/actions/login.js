import {
    LOGIN_SUCCEEDED,
  } from './actionConstants';
  
  export const loginSuccess = data => ({
    type: LOGIN_SUCCEEDED,
    data
  });  